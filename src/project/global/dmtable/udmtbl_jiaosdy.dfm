inherited dmjiaosdy: Tdmjiaosdy
  inherited sp_delete: TUpAdoStoreProc
    ConnectionString = ''
    ProcedureName = 'tbl_sys_jiaosdy_delete'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@bianm'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@operid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@ireturn'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
      end
      item
        Name = '@sreturn'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
      end
      item
        Name = '@scriptver'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
      end>
  end
  inherited sp_append: TUpAdoStoreProc
    ConnectionString = ''
    ProcedureName = 'tbl_sys_jiaosdy_append'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@mingc'
        Attributes = [paNullable]
        DataType = ftString
        Size = 32
      end
      item
        Name = '@miaos'
        Attributes = [paNullable]
        DataType = ftString
        Size = 64
      end
      item
        Name = '@operid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@bianm'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
      end
      item
        Name = '@ireturn'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
      end
      item
        Name = '@sreturn'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
      end
      item
        Name = '@scriptver'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
      end>
  end
  inherited sp_update: TUpAdoStoreProc
    ConnectionString = ''
    ProcedureName = 'tbl_sys_jiaosdy_update'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@bianm'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@mingc'
        Attributes = [paNullable]
        DataType = ftString
        Size = 32
      end
      item
        Name = '@miaos'
        Attributes = [paNullable]
        DataType = ftString
        Size = 64
      end
      item
        Name = '@operid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@ireturn'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
      end
      item
        Name = '@sreturn'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
      end
      item
        Name = '@scriptver'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
      end>
  end
end
