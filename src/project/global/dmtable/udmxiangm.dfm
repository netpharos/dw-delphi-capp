inherited dmxiangm: Tdmxiangm
  inherited sp_delete: TUpAdoStoreProc
    ConnectionString = ''
    ProcedureName = 'tbl_jc_xiangm_delete'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@bianm'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@operid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ireturn'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@sreturn'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@scriptver'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
        Value = Null
      end>
  end
  inherited sp_append: TUpAdoStoreProc
    ConnectionString = ''
    ProcedureName = 'tbl_jc_xiangm_append'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@yonghbm'
        Attributes = [paNullable]
        DataType = ftString
        Size = 32
        Value = Null
      end
      item
        Name = '@mingc'
        Attributes = [paNullable]
        DataType = ftString
        Size = 64
        Value = Null
      end
      item
        Name = '@jianp'
        Attributes = [paNullable]
        DataType = ftString
        Size = 32
        Value = Null
      end
      item
        Name = '@danw'
        Attributes = [paNullable]
        DataType = ftString
        Size = 32
        Value = Null
      end
      item
        Name = '@danj'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 15
        Value = Null
      end
      item
        Name = '@leibbm'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@leibbmqm'
        Attributes = [paNullable]
        DataType = ftString
        Size = 512
        Value = Null
      end
      item
        Name = '@leibmcqm'
        Attributes = [paNullable]
        DataType = ftString
        Size = 512
        Value = Null
      end
      item
        Name = '@operid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@bianm'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@ireturn'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@sreturn'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@scriptver'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
        Value = Null
      end>
  end
  inherited sp_update: TUpAdoStoreProc
    ConnectionString = ''
    ProcedureName = 'tbl_jc_xiangm_update'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@bianm'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@yonghbm'
        Attributes = [paNullable]
        DataType = ftString
        Size = 32
        Value = Null
      end
      item
        Name = '@mingc'
        Attributes = [paNullable]
        DataType = ftString
        Size = 64
        Value = Null
      end
      item
        Name = '@jianp'
        Attributes = [paNullable]
        DataType = ftString
        Size = 32
        Value = Null
      end
      item
        Name = '@danw'
        Attributes = [paNullable]
        DataType = ftString
        Size = 32
        Value = Null
      end
      item
        Name = '@danj'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 15
        Value = Null
      end
      item
        Name = '@leibbm'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@leibbmqm'
        Attributes = [paNullable]
        DataType = ftString
        Size = 512
        Value = Null
      end
      item
        Name = '@leibmcqm'
        Attributes = [paNullable]
        DataType = ftString
        Size = 512
        Value = Null
      end
      item
        Name = '@operid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ireturn'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@sreturn'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 255
        Value = Null
      end
      item
        Name = '@scriptver'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 50
        Value = Null
      end>
  end
end
