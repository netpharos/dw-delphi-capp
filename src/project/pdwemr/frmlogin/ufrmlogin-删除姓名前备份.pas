unit ufrmlogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbdialogbase, DB, ADODB, Buttons, UpSpeedButton, ExtCtrls,
  UPanel, StdCtrls, UpEdit, UpComboBox, utypes, uglobal, Menus,
  UpPopupMenu, frxClass, frxDesgn, frxDBSet, UpDataSource, UpAdoTable,
  UpAdoQuery, UpLabel, UpImage, UpCheckBox, ucsmsg, IdUDPClient,
  umysystem, UpUdp, WinSock, frxExportXLS, frxExportPDF, udmoperator, jpeg;
                         
type
  Tfrmlogin = class(Tfrmdbdialogbase)
    chkjiydenglxx: TUpCheckBox;
    edtyuangbm: TUpEdit;
    edtxingm: TUpEdit;
    edtkoul: TUpEdit;
    img1: TImage;
    btn1: TUpSpeedButton;
    procedure btnyesClick(Sender: TObject);
    procedure btnquitClick(Sender: TObject);
    procedure edtkoulKeyPress(Sender: TObject; var Key: Char);
    procedure cbbxingmChange(Sender: TObject);
    procedure chkjiydenglxxClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure edtyuangbmKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    dmoperator : Tdmoperator;
    //登录员工对象

    denglsbcs : Integer;
    //登录失败次数               
  public
    bianm : Integer;
    //登录员工编码
  public
    function Execute_dll_showmodal_before() : Boolean; override;
    function Execute_dll_showmodal_after() : Boolean; override;
  
  end;

var
  frmlogin: Tfrmlogin;

implementation

{$R *.dfm}

{ Tfrmlogin }

function Tfrmlogin.Execute_dll_showmodal_after: Boolean;
begin
  Result := bianm <> -1;
end;

function Tfrmlogin.Execute_dll_showmodal_before: Boolean;
begin
  //成功登录员工编码
  bianm := -1;
  //尝试登录次数初始化
  self.denglsbcs := 0;

  //创建登录员工对象
  if not Assigned(dmoperator) then
    dmoperator := Tdmoperator.Create(@dllparams);
  
  Result := True;
end;

procedure Tfrmlogin.btnyesClick(Sender: TObject);  
var
  str : string;
begin
  //inherited;

  { 输入验证；
  }
  //判断用户选择和输入是否合法
  if Trim(edtxingm.Text) = '' then
  begin
    baseobj.showwarning('请输入员工代码！');
    edtyuangbm.SetFocus;
    Exit;
  end;

  { 口令验证
  }
  if not dmoperator.password_check(edtyuangbm.Text, edtkoul.Text) then
  begin  
    Inc(denglsbcs);
    baseobj.showwarning('口令验证失败,请重新输入!');
    edtkoul.Clear;
    edtkoul.SetFocus;
  end
  else
  begin
    bianm := dmoperator.ioperid;
    ModalResult := mrOk;
  end;
  
  if denglsbcs >= 3 then
  begin
    errmsg := '口令错误,尝试次数超过3次,系统自动关闭!';
    ModalResult := mrCancel;
  end;
end;

procedure Tfrmlogin.btnquitClick(Sender: TObject);
begin
  inherited;

  ModalResult := mrCancel;
  
  errmsg := '用户取消登录!';
end;

procedure Tfrmlogin.edtkoulKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  //回车键模拟成确定按钮
  if Key = #13 then
  begin
    btnyesClick(Sender);
  end;
end;

procedure Tfrmlogin.cbbxingmChange(Sender: TObject);
begin
  inherited;

  //清空密码字段
  edtkoul.Text := '';
end;

procedure Tfrmlogin.chkjiydenglxxClick(Sender: TObject);
begin
  inherited;

  if chkjiydenglxx.Checked then
  begin
    gmysystem.mainconfig.WriteString('localparams','userid_default',edtyuangbm.Text); 
  end;
end;

procedure Tfrmlogin.FormDestroy(Sender: TObject);
begin
  inherited;
        
  //销毁操作员对象
  if Assigned(dmoperator) then
    FreeAndNil(dmoperator);
end;

procedure Tfrmlogin.edtyuangbmKeyPress(Sender: TObject; var Key: Char);
var
  username : string;
begin
  inherited;

  //回车键模拟成确定按钮
  if Key = #13 then
  begin
    if not dmoperator.getusernamebyuserid(edtyuangbm.Text, username) then
    begin
      baseobj.showerror('获取用户信息失败:' + dmoperator.errmsg);
      edtyuangbm.Text := '';
      Exit;
    end
    else
    begin
      edtxingm.Text := username;
      edtkoul.Text := '';
      edtkoul.SetFocus;
    end;
  end;
end;

procedure Tfrmlogin.FormShow(Sender: TObject);
begin
  inherited;

  edtyuangbm.SetFocus;
end;

end.
