unit ufrmxitgl_menzyspb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbgridbase, ADODB, UpAdoStoreProc, Menus, UpPopupMenu,
  frxClass, frxDesgn, frxDBSet, UpDataSource, DB, UpAdoTable, UpAdoQuery,
  Grids, Wwdbigrd, Wwdbgrid, UpWWDbGrid, ExtCtrls, Buttons, UpSpeedButton,
  StdCtrls, UpComboBox, UpLabel, UPanel, frxExportXLS, frxExportPDF;

type
  Tfrmxitgl_menzyspb = class(Tfrmdbgridbase)
    ptop: TUPanel;
    lblkes: TUpLabel;
    cbbkes: TUpComboBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbbkesChange(Sender: TObject);
  private
    procedure filterYis();
    //过滤医生信息
  protected
    function Execute_dll_show_before() : Boolean; override;
  end;

var
  frmxitgl_menzyspb: Tfrmxitgl_menzyspb;

implementation

{$R *.dfm}

function Tfrmxitgl_menzyspb.Execute_dll_show_before: Boolean;
begin
  inherited Execute_dll_show_before();

  //根据门诊医生排班权限加载科室信息
  //是否有全院门诊医生排班权限；系统管理员有全院权限;
  if (dllparams.mysystem^.loginyuang.bianm = 0) or
     (dmtbl_public.yisIsHasMenzyspbQX_Quany(dllparams.mysystem^.loginyuang.bianm)) then
  begin
    //添加允许挂号科室
    dmtbl_public.fillcbb_kes_guah(@cbbkes);
  end
  else if dmtbl_public.yisIsHasMenzyspbQX_Kes(dllparams.mysystem^.loginyuang.bianm) then
  begin
    cbbkes.AddItemUP(dllparams.mysystem^.loginyuang.kesmc,
        IntToStr(dllparams.mysystem^.loginyuang.kesbm));
  end;

  //打开医生表
  tbl.Close;
  tbl.Open;
  tbl.Filter := 'kesbm=0';
  tbl.Filtered := True;

  Result := True;
end;

procedure Tfrmxitgl_menzyspb.filterYis;
begin
  tbl.Filtered := False;
  tbl.Filter := 'kesbm=' + cbbkes.GetItemValueDefault('0');
  tbl.Filtered := True;
end;

procedure Tfrmxitgl_menzyspb.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  frmxitgl_menzyspb := nil;
end;

procedure Tfrmxitgl_menzyspb.cbbkesChange(Sender: TObject);
begin
  inherited;

  //刷新医生
  filterYis;
end;

end.
