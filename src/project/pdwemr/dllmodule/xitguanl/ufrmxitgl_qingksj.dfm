inherited frmxitgl_qingksj: Tfrmxitgl_qingksj
  Left = 336
  Top = 124
  Caption = #25968#25454#28165#31354
  ClientHeight = 446
  ClientWidth = 690
  PixelsPerInch = 96
  TextHeight = 16
  object p1: TUPanel [0]
    Left = 0
    Top = 36
    Width = 690
    Height = 410
    Align = alClient
    ParentColor = True
    TabOrder = 1
    object lbl1: TUpLabel
      Left = 216
      Top = 34
      Width = 360
      Height = 16
      Caption = #25968#25454#28165#31354#23558#28165#38500#25152#26377#30340#31995#32479#19994#21153#25968#25454','#35831#35880#24910#20351#29992#65281
    end
    object lbl2: TUpLabel
      Left = 155
      Top = 128
      Width = 136
      Height = 16
      Caption = #35831#36755#20837#31649#29702#21592#21475#20196':'
    end
    object btnqingksjyz: TUpSpeedButton
      Left = 403
      Top = 125
      Width = 96
      Height = 22
      Caption = #30830#23450
      Glyph.Data = {
        92010000424D9201000000000000920000002800000010000000100000000100
        08000000000000010000120B0000120B0000170000001700000000000000FFFF
        FF00FF00FF0000660000149D210019AA2B00179D27001AB02D001BA92E001DB1
        32001EB231001FB133001EAE310022B7380021B4370025A83B0031C24F0031B5
        4D003BCB5E0041C5630047D36D004FD6790053DE7F0002020202020202020202
        0202020202020202020202020202020202020202020202020202020303020202
        02020202020202020202030A0B03020202020202020202020203100D070E0302
        02020202020202020314120F030809030202020202020203151611030203060C
        0302020202020202031303020202020305030202020202020203020202020202
        0304030202020202020202020202020202020303020202020202020202020202
        0202020303020202020202020202020202020202020202020202020202020202
        0202020202020202020202020202020202020202020202020202020202020202
        02020202020202020202020202020202020202020202}
      OnClick = btnqingksjyzClick
      alignleftorder = 0
      alignrightorder = 0
    end
    object edtkoul: TUpEdit
      Left = 295
      Top = 125
      Width = 102
      Height = 24
      ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
      PasswordChar = '*'
      TabOrder = 0
      AutoInit = False
      HistoryValueOnOff = True
    end
  end
  object p2: TUPanel [1]
    Left = 0
    Top = 36
    Width = 690
    Height = 410
    Align = alClient
    ParentColor = True
    TabOrder = 2
    object lbl4: TUpLabel
      Left = 33
      Top = 23
      Width = 104
      Height = 16
      Caption = #35831#36755#20837#39564#35777#30721':'
    end
    object imgyanzm: TCnValidateImage
      Left = 148
      Top = 13
      Width = 143
      Height = 35
      Color = clBtnFace
      ParentColor = False
      OnClick = imgyanzmClick
    end
    object btnqingksj: TUpSpeedButton
      Left = 296
      Top = 71
      Width = 96
      Height = 32
      Caption = #30830#23450
      Glyph.Data = {
        92010000424D9201000000000000920000002800000010000000100000000100
        08000000000000010000120B0000120B0000170000001700000000000000FFFF
        FF00FF00FF0000660000149D210019AA2B00179D27001AB02D001BA92E001DB1
        32001EB231001FB133001EAE310022B7380021B4370025A83B0031C24F0031B5
        4D003BCB5E0041C5630047D36D004FD6790053DE7F0002020202020202020202
        0202020202020202020202020202020202020202020202020202020303020202
        02020202020202020202030A0B03020202020202020202020203100D070E0302
        02020202020202020314120F030809030202020202020203151611030203060C
        0302020202020202031303020202020305030202020202020203020202020202
        0304030202020202020202020202020202020303020202020202020202020202
        0202020303020202020202020202020202020202020202020202020202020202
        0202020202020202020202020202020202020202020202020202020202020202
        02020202020202020202020202020202020202020202}
      OnClick = btnqingksjClick
      alignleftorder = 0
      alignrightorder = 0
    end
    object edtyanzm: TUpEdit
      Left = 303
      Top = 20
      Width = 102
      Height = 24
      ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
      TabOrder = 0
      AutoInit = False
      HistoryValueOnOff = True
    end
  end
  inherited pbuttons: TUPanel
    Width = 690
    inherited btnrefresh: TUpSpeedButton
      Left = 141
      Visible = False
    end
    inherited btnadd: TUpSpeedButton
      Left = 227
      Visible = False
    end
    inherited btnupdate: TUpSpeedButton
      Left = 312
      Visible = False
    end
    inherited btndelete: TUpSpeedButton
      Left = 397
      Visible = False
    end
    inherited btnprint: TUpSpeedButton
      Left = 652
      Visible = False
    end
    inherited btnquit: TUpSpeedButton
      Left = 5
      alignleftControl = nil
      alignleftorder = 0
    end
    inherited btncancel: TUpSpeedButton
      Left = 482
      Visible = False
    end
    inherited btnsave: TUpSpeedButton
      Left = 567
      Visible = False
    end
  end
end
