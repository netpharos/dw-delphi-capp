{ 项目选择窗体
}
unit ufrmcomm_selxiangm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbselectbase, Menus, UpPopupMenu, frxClass, frxDesgn,
  frxDBSet, UpDataSource, DB, ADODB, UpAdoTable, UpAdoQuery, Grids,
  Wwdbigrd, Wwdbgrid, UpWWDbGrid, StdCtrls, UpEdit, UpLabel, Buttons,
  UpSpeedButton, ExtCtrls, UPanel, frxExportXLS, frxExportPDF, ToolPanels,
  UpAdvToolPanel, ComCtrls, UpTreeView, ImgList, udbtree, AdvSplitter,
  DBGrids;

type
  Tfrmcomm_selxiangm = class(Tfrmdbselectbase)
    p1: TUpAdvToolPanel;
    qryxiangmtree: TUpAdoQuery;
    imglstilimglisttree: TImageList;
    tvxiangm: TUpTreeView;
    advspltr1: TAdvSplitter;
    dstask: TDataSource;
    qrytask: TUpAdoQuery;
    dbgtask: TDBGrid;
    procedure edtseljianpKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tvxiangmChange(Sender: TObject; Node: TTreeNode);
    procedure tvxiangmGetImageIndex(Sender: TObject; Node: TTreeNode);
    procedure btnselectClick(Sender: TObject);
    procedure edtseljianpKeyPress(Sender: TObject; var Key: Char);
    procedure qryAfterScroll(DataSet: TDataSet);
    procedure qrytaskAfterOpen(DataSet: TDataSet);

  private      
    dbtree : TDBTree;   
    function getnodename_showtreenode() : string;
    //显示目录树获取节点显示名称回调函数

    procedure loadXiangm(bumbm: integer);
    //加载项目列表
    procedure loadRenw(xiangmdh: string);
    //加载任务

  protected
    function Execute_dll_showmodal_before() : Boolean; override;
    function Execute_dll_showmodal_after() : Boolean; override;
  public
    fxiangmdh: string;
    fxiangmmc: string;
    fyanzjd: string;
    fxiangmid: string;
    fxiangmglbmid: string;
    frenwid: string;
    frenwmc: string;
  end;

implementation

{$R *.dfm}

{ Tfrmcomm_selxiangm }

function Tfrmcomm_selxiangm.Execute_dll_showmodal_after: Boolean;
begin
  Result := False;

  if ModalResult = mrCancel then Exit;

  if qry.isEmpty then Exit;

  fxiangmdh:= qry.getString('xiangmdh');
  fxiangmmc:= qry.getString('xiangmmc');
  fyanzjd:= qry.getString('yanzjd');
  fxiangmid:= qry.getString('yulzf1');
  fxiangmglbmid:= qry.getString('bumid');
  //任务ID、任务名称
  if qrytask.RecordCount>0 then
  begin
    frenwid:= qrytask.getString(dllparams.mysystem.global_sysparams.valueitem('mpmtaskid_fieldname'));
    frenwmc:= qrytask.getString(dllparams.mysystem.global_sysparams.valueitem('mpmtaskname_fieldname'));
  end
  else
  begin
    frenwid:= '';
    frenwmc:= '';
  end;

  Result := True;
end;

function Tfrmcomm_selxiangm.Execute_dll_showmodal_before: Boolean;
begin
  //不采用父类的处理方法
  //inherited;
                 
  //显示项目树
  if not Assigned(dbtree) then
  begin
    //创建树操作对象
    dbtree := TDBTree.Create(qryxiangmtree, 'intf_mpm_jc_bum', tvxiangm, dllparams.padoconn);
    dbtree.fgetnodename_showtreenode := getnodename_showtreenode;
  end;
  dbtree.wherestr:= ' where zhuangt=1';
  //显示树
  dbtree.showtree;

  result := true;
end;

procedure Tfrmcomm_selxiangm.edtseljianpKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  //inherited;

  if key=13 then
  begin
    if trim(edtseljianp.Text)<>'' then
    begin
      qry.OpenSql(format('select * from intf_mpm_ls_xiangm' +
        ' where xiangmdh like ''%s%s%s''',
        ['%', trim(edtseljianp.Text), '%']));
    end;
  end;
end;

procedure Tfrmcomm_selxiangm.tvxiangmChange(Sender: TObject;
  Node: TTreeNode);
begin
  inherited;

  //树当前选中节点改变,通知qry改变当前记录
  dbtree.treechange(node);

  { 过滤项目
  }
  { 只在选中分类下的项目中过滤}
  if tvxiangm.Selected<>nil then
  begin
    loadXiangm(qryxiangmtree.getInteger('bianm'));
  end
  else
  begin
    loadXiangm(0);
  end;
end;

function Tfrmcomm_selxiangm.getnodename_showtreenode: string;
begin
  Result := qryxiangmtree.fieldbyname('mingc').AsString;
end;

procedure Tfrmcomm_selxiangm.tvxiangmGetImageIndex(Sender: TObject;
  Node: TTreeNode);
begin
  inherited;

  { 改变节点图标
  }
  if Node.Selected then
  begin
    Node.ImageIndex := 1;
  end
  else
  begin
    Node.ImageIndex := 2;
  end;
end;

procedure Tfrmcomm_selxiangm.btnselectClick(Sender: TObject);
var
  key: Word;
begin
  inherited;

  key:= 13;
  edtseljianpKeyUp(edtseljianp, key, []);
end;

procedure Tfrmcomm_selxiangm.loadXiangm(bumbm: integer);
var
  bumid: string;
begin
  qry1.OpenSql(format('select yonghbm from intf_mpm_jc_bum' +
      ' where bianm=%d', [bumbm]));
  if qry1.isEmpty then
    bumid:= ''
  else
    bumid:= qry1.getString('yonghbm');
  qry1.Close;

  if bumid='' then
  begin
    qry.OpenSql(format('select * from intf_mpm_ls_xiangm' +
      ' where 1=2', []));
  end
  else
  begin
    qry.OpenSql(format('select * from intf_mpm_ls_xiangm' +
      ' where bumid=''%s''', [bumid]));
  end;
end;

procedure Tfrmcomm_selxiangm.edtseljianpKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;     
  
  if key='''' then
  begin
    key:= chr(0);
    exit;
  end;
end;

procedure Tfrmcomm_selxiangm.loadRenw(xiangmdh: string);
var
  strsql: string;
begin
  strsql:= format(dllparams.mysystem.global_sysparams.valueitem('mpmtaskselsql'),
    [xiangmdh]);

  qrytask.OpenSql(strsql);
end;


procedure Tfrmcomm_selxiangm.qryAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if qry.RecordCount>0 then
  begin
    loadRenw(qry.getString(dllparams.mysystem.global_sysparams.valueitem('mpmtaskrelxiangmfieldname')));
  end
  else
  begin
    loadRenw('');
  end;
end;

procedure Tfrmcomm_selxiangm.qrytaskAfterOpen(DataSet: TDataSet);
var
  i: integer;
begin
  inherited;

  for i:=0 to dbgtask.Columns.Count-1 do
  begin
    dbgtask.Columns[i].Width:= 260;
    dbgtask.Columns[i].Alignment:= taCenter;
  end;
end;

end.
