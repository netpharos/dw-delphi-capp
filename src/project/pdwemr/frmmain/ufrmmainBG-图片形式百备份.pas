{
  导航面板顺序说明：
    0---门诊业务
    1---住院业务
    2---医技业务
    3---药库业务
    4---药房业务
    5---物资库业务
    6---财务管理
    7---院长查询
    8---数据字典
    9---基础数据
    10---系统管理
}

unit ufrmmainBG;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, SHDocVw, ExtCtrls, Buttons, UpSpeedButton, StdCtrls,
  GradientLabel, UpLabel, UPanel, RzTabs, dxGDIPlusClasses,
  umysystem, udlltransfer, udllfunctions, utypes, ufrmdbbase, frxExportXLS,
  frxClass, frxExportPDF, Menus, UpPopupMenu, frxDesgn, frxDBSet,
  UpDataSource, DB, ADODB, UpAdoTable, UpAdoQuery;

type
  TfrmmainBG = class(Tfrmdbbase)
    wb: TWebBrowser;
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private

  protected
    function Execute_dll_show_before() : Boolean; override;

  public
    ffrmmainhandle : HWND;
    //主窗体句柄

    procedure changeNav(str : string);
    //主窗体通知切换导航面板
  end;

var
  frmmainBG: TfrmmainBG;

implementation

{$R *.dfm}

{ TfrmmainBG }

procedure TfrmmainBG.FormShow(Sender: TObject);
begin
  self.WindowState := wsMaximized;
end;

procedure TfrmmainBG.FormActivate(Sender: TObject);
begin
  self.WindowState := wsMaximized;
end;

procedure TfrmmainBG.changeNav(str: string);
begin

end;

function TfrmmainBG.Execute_dll_show_before: Boolean;
begin
  result := inherited Execute_dll_showmodal_before();

  wb.Navigate('file:///' + ExtractFilePath(Application.ExeName) + 'web/index.html');
end;

procedure TfrmmainBG.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;

  frmmainBG := nil;
end;

end.
