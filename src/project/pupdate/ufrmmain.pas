unit ufrmmain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, UpAdoQuery, UpAdoConnection, ExtCtrls, UPanel,
  StdCtrls, UpLabel, UpListBox, Menus, UpPopupMenu, ufrmbase,
  ShellAPI, Registry, Buttons, UpSpeedButton, umyconfig, ComCtrls, ulog;

type
  Tfrmmain = class(TFrmBase)
    adoconn: TUpAdoConnection;
    qry: TUpAdoQuery;
    qryfile: TUpAdoQuery;
    p1: TUPanel;
    lbl1: TUpLabel;
    lblshengjxh: TUpLabel;
    lstfile: TUpListBox;
    lbl2: TUpLabel;
    atncfldqryfilebianm: TAutoIncField;
    intgrfldqryfileshengjxh: TIntegerField;
    strngfldlxmcqryfileshiykhbm: TStringField;
    strngfldlxmcqryfilewenjmc: TStringField;
    intgrfldqryfilewenjdx: TIntegerField;
    strngfldlxmcqryfilewenjccml: TStringField;
    dbfieldqryfilewenj: TBlobField;
    intgrfldqryfileshifljyx: TIntegerField;
    intgrfldqryfilexiazcs: TIntegerField;
    btnbegin: TUpSpeedButton;
    stb: TStatusBar;
    qry1: TUpAdoQuery;
    procedure FormCreate(Sender: TObject);
    procedure btnbeginClick(Sender: TObject);
  private
    falreadyRead : Boolean;  //更新信息是否已读取
    fbeginupdate : Boolean;  //开始更新标志
    FUpdateCurrDir : string;

    fminversion_server : Integer;
    //服务器最小版本号；
    //检查当前客户端是否漏了已经被服务器删除的更新；
    
    procedure readupdateinfo();
    //读取更新信息
    procedure systemupdate(shengjid : string);
    //系统更新
    procedure writeshengjxh2local();
    //写升级序号到本地
  public
    //升级服务器信息
    fshengjxh : string;
    fshengjxh_local : string;
    fdbserver : string;
    fdbuser : string;
    fdbpassword :string;
    fdbdatabase :string;

    function init() : Boolean;
    //初始化

  public
    errcode: integer;
    errmsg: string;

  end;

var
  frmmain: Tfrmmain;

implementation

{$R *.dfm}

{ Tfrmmain }

function Tfrmmain.init: Boolean;
begin
  Result := False;
  
  try
    FUpdateCurrDir:= ExtractFilePath(application.ExeName);

    log.appendlog('系统安装路径:' + FUpdateCurrDir);
  except
    on E:Exception do
    begin
      errmsg := e.Message;
      Exit;
    end;
  end;

  try
    if adoconn.Connected then adoconn.Close;

    adoconn.ConnectionString := 'Provider=SQLOLEDB.1; ' +
        'Password=' + fdbpassword + '; ' +
        'Persist Security Info=True; ' +
        'User ID=' + fdbuser + '; ' +
        'Initial Catalog=' + fdbdatabase + '; ' +
        'Data Source=' + fdbserver;
    adoconn.Open();

    lblshengjxh.Caption := fshengjxh_local + '---' + fshengjxh;

    //读取更新信息
    readupdateinfo();
  except
    errmsg := '连接升级服务器失败！';
    Exit;
  end;

  try
    //$$$服务器最新版本号；检查本地版本号是否低于服务器最新号版本；
    //是则表示软件版本过旧，需要维护升级；
    qry.OpenSql('select isnull(max(shengjxh),0) from xt_shengjjl');
    fminversion_server := qry.getInteger(0);
    qry.Close;

    if StrToInt(fshengjxh_local)>=fminversion_server then
    begin
      raise Exception.Create('本地系统组件已经是最新版本，不需要升级！');
    end;
  except
    on e:Exception do
    begin
      errmsg := e.Message;
      Exit;
    end;
  end;

  Result := True;
end;

procedure Tfrmmain.FormCreate(Sender: TObject);
begin
  falreadyRead := False;
  fbeginupdate := False;
end;

procedure Tfrmmain.systemupdate(shengjid : string);
var
  filename : string;
begin
  qryfile.OpenSql('select * from xt_shengjwjqd ' +
    ' where shengjxh=' + shengjid);
  qryfile.First;
  while not qryfile.Eof do
  begin
    if not dbfieldqryfilewenj.IsNull then
    begin
      filename := FUpdateCurrDir + '\' + qryfile.getString('wenjccml') + '\' +
        qryfile.getString('wenjmc');

      log.appendlog('下载文件:' + filename);
      stb.SimpleText := '正在下载文件' + qryfile.getString('wenjmc');
      Application.ProcessMessages;

      //文件下载次数++
      qry1.ExecuteSql('update xt_shengjwjqd set xiazcs=xiazcs+1 ' +
          ' where bianm=' + qryfile.getString('bianm'));

      dbfieldqryfilewenj.SaveToFile(filename);
    end;

    qryfile.Next;
  end;
  qryfile.Close;
  stb.SimpleText := '文件下载完成！';
  Application.ProcessMessages;
  
  //客户端写入最新升级序号
  log.appendlog('本地写入本次升级点:' + shengjid);
  writeshengjxh2local;

  //$$$运行升级后立即执行文件；目前只能运行一个文件；
  qry.OpenSql('select wenjccml, wenjmc from xt_shengjwjqd ' +
    ' where shengjxh=' + fshengjxh +
    '   and shifljyx=1');
  if qry.RecordCount > 0 then
  begin
    //有多个可执行文件,目前版本是只执行一个文件
    filename := FUpdateCurrDir + '\' +
        qry.getString('wenjccml') + '\' +
        qry.getString('wenjmc');
    log.appendlog('升级后执行文件:' + filename);
    ShellExecute(0, 'open', PChar(filename),'','',SW_SHOW);
  end;
end;

procedure Tfrmmain.readupdateinfo;
var
  filename : string;
  i, ibegin, iend : Integer;
begin
  if falreadyRead then Exit;
  falreadyRead := True;

  lstfile.Items.Clear;
  lstfile.Items.Add('myhisclient安装目录：' + FUpdateCurrDir);
  lstfile.Items.Add('本次更新文件:');

  //从给本地版本下一个版本开始
  ibegin := StrToInt(fshengjxh_local)  + 1;
  iend := StrToInt(fshengjxh);
  for i:=ibegin to iend do
  begin
    lstfile.Items.Add('升级号:' + IntToStr(i));
    qry.OpenSql('SELECT xs.bianm,xs.shengjxh,xs.shiykhbm,xs.wenjmc,' +
      ' xs.wenjdx,xs.wenjccml,xs.shifljyx,xs.xiazcs ' +
      ' FROM xt_shengjwjqd xs ' +
      ' WHERE xs.shengjxh=' + IntToStr(i));
    qry.First;
    while not qry.Eof do
    begin
      filename := '\' + qry.getString('wenjccml') + '\' + qry.getString('wenjmc');
      lstfile.Items.Add(filename);

      qry.Next;
    end;
    qry.Close;
  end;
end;

procedure Tfrmmain.btnbeginClick(Sender: TObject);
var
  i, ibegin, iend : Integer;
begin
  inherited;
  
  //$$$是否已经开始更新
  if fbeginupdate then Exit;
  fbeginupdate := True;

  ibegin := StrToInt(fshengjxh_local) + 1;
  iend := StrToInt(fshengjxh);

  for i:=ibegin to iend do
  begin
    log.appendlog('当前升级点:' + IntToStr(i));
    systemupdate(IntToStr(i));

    sleep(1000*15);
  end;

  log.appendlog('升级完成！');
  stb.SimpleText := '系统更新完成！';
  Application.ProcessMessages;
  ShowMessage('系统更新完毕,请重新登录！');
   
  //系统更新完成,重新调用myhisclient
  //myhisclientfilename := fmyhisclient_path + 'pmyhisclient.exe';
  //ShellExecute(0, 'open', PChar(myhisclientfilename),'','',SW_SHOW);

  //关闭更新程序
  close;
end;

procedure Tfrmmain.writeshengjxh2local;
var
  myhisclient_ini : string;
  config : TMyConfig;
begin
  myhisclient_ini := FUpdateCurrDir + '\' + 'ppdfreview.ini';
  config := TMyConfig.Create(myhisclient_ini);
  config.WriteString('system','sys_update_id',fshengjxh);
  FreeAndNil(config);
end;

end.

