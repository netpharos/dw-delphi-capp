unit ufrmmain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmbase, Menus, UpPopupMenu, DB, ADODB, UpAdoConnection,
  umyconfig, udbconfigfile, ExtCtrls, UPanel, StdCtrls, UpLabel,
  UpListBox, ComCtrls, Buttons, UpSpeedButton, Registry,
  ShellAPI, UpAdoQuery, updos, AdvSplitter;

type
  Tfrmmain = class(TFrmBase)
    adocon: TUpAdoConnection;
    ptop: TUPanel;
    lblshengjxh: TUpLabel;
    lstfile: TUpListBox;
    btnbegin: TUpSpeedButton;
    btnCANCEL: TUpSpeedButton;
    qry: TUpAdoQuery;
    qryfile: TUpAdoQuery;
    atncfldqryfilebianm: TAutoIncField;
    qryfileshengjxh: TIntegerField;
    strngfldqryfileshiykhbm: TStringField;
    strngfldqryfilewenjmc: TStringField;
    qryfilewenjdx: TIntegerField;
    strngfldqryfilewenjccml: TStringField;
    blbfldqryfilewenj: TBlobField;
    qryfileshifljyx: TIntegerField;
    qryfilexiazcs: TIntegerField;
    splt1: TAdvSplitter;
    mmo: TMemo;
    qry1: TUpAdoQuery;
    procedure btnbeginClick(Sender: TObject);
    procedure btnCANCELClick(Sender: TObject);
  private
    fshengjxh_prev : string; //上次升级序号
    fshengjxh : string; //升级序号
    fserver_path : string; //服务器安装路径
    frunfilename : string; //升级后执行文件名

    procedure addresultstr(strmsg : string);

    function connectDb() : Boolean;
    //连接数据库服务器

    function readshengjinfo() : Boolean;
    //读取升级信息

    procedure backupdatabase();
    //备份数据库

    function updatefile() : Boolean;
    //升级文件
    procedure savefile2db(bianm : Integer; filename : string);
    //保存文件到db

    procedure exec_sql_file(filename : string);
    //运行sql文件

  public
    function init() : Boolean;
  
  end;

var
  frmmain: Tfrmmain;

implementation

{$R *.dfm}

{ Tfrmmain }

function Tfrmmain.connectDb: Boolean;
var
  mainconfig : string;
  server_ini : string;
  config : TMyConfig;
  dbconfilename : string;
  reg : TRegistry;
  str, strconnectionstring: string;
  dbconfigfile : TDBConfigFile;
  buffer : array[0..1024] of char;
  len : integer;
begin
  try
    mainconfig := ExtractFilePath(Application.ExeName) + '/pmakeupdate.ini';
    config := TMyConfig.Create(mainconfig);
    dbconfilename := config.ReadString('system','dbconnfile','');
    FreeAndNil(config);
    if dbconfilename = '' then
    begin
      raise Exception.Create('读取数据库连接参数失败！');
    end;
    
    dbconfigfile := TDBConfigFile.create(ExtractFilePath(Application.ExeName) + dbconfilename);
    zeromemory(@buffer, 1024);
    if not dbconfigfile.readconfigstring(buffer, len) then
    begin
      str := '读取业务数据库配置串失败：' + inttostr(dbconfigfile.errcode) + '---' + dbconfigfile.errmsg;
      freeandnil(dbconfigfile);
      raise Exception.Create(str);
    end;
    freeandnil(dbconfigfile); 

    setlength(strconnectionstring, len);
    copymemory(pchar(strconnectionstring), @buffer, len);

    if adocon.Connected then
      adocon.close;
    adocon.ConnectionString := strconnectionstring;
    adocon.Open();

    Result := True;
  except
    on E:Exception do
    begin
      errmsg := e.Message;
      Result := False;
    end;
  end;
end;

procedure Tfrmmain.btnbeginClick(Sender: TObject);
begin
  inherited;

  //升级前先备份数据库
  //2013-11-17：数据库很大的时候备份不可行了;
  //addresultstr('升级前备份数据库...');
  //backupdatabase;
  //addresultstr('升级前备份数据库成功！');

  //升级文件
  if not updatefile then
  begin
    baseobj.showerror('升级失败:' + errmsg);
    Exit;
  end;
end;

procedure Tfrmmain.btnCANCELClick(Sender: TObject);
begin
  inherited;

  Close();
end;

function Tfrmmain.updatefile: Boolean;
var
  bianm : integer;
  filepath, filename : string;     
  config : TMyConfig;
  i : integer;
  blijyx : integer; //文件是否在升级后立即运行
  str : string;
  save2db : Integer;
  updatedir : string;
  bissqlfile : integer;
  ipupsql_exitcode : Cardinal; //pupsql返回结果;
  strpupsql_outline : string;  //pupsql输出结果;

  dbserver, dbuser, dbpassword, dbdatabase : string;
begin
  Result := False;

  try
    //启动事务
    adocon.BeginTrans;

    //$$$查找升级配置文件
    filepath := ExtractFilePath(Application.ExeName) + '\';
    filename := filepath + 'pmakeupdate.ini';
    if not FileExists(filename) then
    begin
      raise exception.Create('升级配置文件未找到！');
    end;                 
    config := TMyConfig.Create(filename);


    //$$$新增升级批次信息
    qry.ExecuteSql('insert into xt_shengjjl(shengjxh,banbh,shengjrq,beiz) ' +
      ' values(' + fshengjxh + ','''',getdate(),'''')');

    //逐个处理升级文件
    save2db := 0;
    updatedir := '';
    for i:=0 to lstfile.Items.Count-1 do
    begin
      //升级文件
      filename := lstfile.Items[i];
      //是否保存到数据库
      save2db := config.ReadInteger(lstfile.Items[i], 'save2db',0);
      //是否是sql文件
      bissqlfile := config.ReadInteger(lstfile.Items[i], 'issqlfile', 0);
      //升级后存放路径，相对于app根目录
      updatedir := config.ReadString(lstfile.Items[i], 'updatedir', '');
      //是否立即运行
      blijyx := config.ReadInteger(lstfile.Items[i], 'lijyunx', 0);

      if Trim(filename) = '' then
      begin
        //非法选项,不处理
        save2db := 0;
        updatedir := '';
        Continue;
      end;

      addresultstr('正在升级文件' + filename);
      Application.ProcessMessages;

      case save2db of
        1: //存储到数据库
        begin 
          //保存到数据库
          qry.ExecuteSql('insert into xt_shengjwjqd(shengjxh,shiykhbm,wenjmc,' +
              'wenjdx,wenjccml,shifljyx) ' +
              ' values(' + fshengjxh +','''',''' + filename + ''',0,''' +
              updatedir + ''',' + IntToStr(blijyx) + ')');
          qry.OpenSql('SELECT @@IDENTITY');
          bianm := qry.getInteger(0);

          savefile2db(bianm,filepath + filename);   
        end;
        2: //存储到本地;服务器文件升级;
        begin
          //$$$2018-10-09
          //服务器升级方式废弃，直接采用人工手工升级，不采用此程序升级
          //这里相关的逻辑废弃了！！！
          //复制文件到服务器安装目录
          if not CopyFile(PChar(filepath+filename),
              PChar(fserver_path+'\'+updatedir+'\'+filename), False) then
          begin
            raise Exception.Create('复制文件' + updatedir+'\'+filename +'到服务器目录失败！');
          end; 
        end;
        3: //不带存储属性;检查是否是sql文件
        begin  //是否是sql文件需要立刻运行
          //$$$2018-10-09
          //服务器升级方式废弃，直接采用人工手工升级，不采用此程序升级
          //这里相关的逻辑废弃了！！！
          if bissqlfile = 1 then
          begin
            //执行sql脚本文件
            exec_sql_file(filepath + filename);
            {
            //获取数据库连接参数
            if not baseobj.getDbLoingIngo(adocon.ConnectionString, dbserver, dbuser, dbpassword, dbdatabase) then
              raise Exception.Create('执行sql补丁时获取数据库连接参数失败');

            addresultstr(filepath + 'pupsql.exe ' + dbserver + ' ' + dbuser + ' ' + dbpassword + ' ' +
                dbdatabase + ' file "' + filepath + filename +'"');
            strpupsql_outline := RunDOS(filepath + 'pupsql.exe ' + dbserver + ' ' + dbuser + ' ' + dbpassword + ' ' +
                dbdatabase + ' file "' + filepath + filename  + '"', ipupsql_exitcode);
            addresultstr(strpupsql_outline);
            if ipupsql_exitcode <> 0 then
            begin
              raise Exception.Create('执行sql补丁失败:' + IntToStr(ipupsql_exitcode) + '-' + strpupsql_outline);
            end;
            }
          end;
        end;
        else
        begin
          //非法选项,不处理
          save2db := 0;
          updatedir := '';
          Continue;         
        end;
      end;
    end;

    //提交事务
    adocon.CommitTrans;
              
    //$$$执行升级成功后文件；这里也是采用手工方式了;2018-10-09;
    if frunfilename <> '' then
    begin
      str := ExtractFilePath(Application.ExeName) + '\' + frunfilename;
      ShellExecute(0, 'open', PChar(str), '', '', SW_SHOW);
    end;
                                      
    Result := True;
    
    baseobj.showmessage('升级成功！');
    btnbegin.Enabled := False;
  except
    on E:Exception do
    begin
      //回滚事务
      adocon.RollbackTrans;
      errmsg := e.Message
    end;
  end;
end;

procedure Tfrmmain.savefile2db(bianm: Integer; filename: string);
var
  filestream : TMemoryStream;
begin
  if not FileExists(filename) then
    Exception.Create('文件' + filename + '不存在！');

  try
    filestream := TMemoryStream.Create;
    filestream.LoadFromFile(filename);
    try
      qryfile.Close;
      qryfile.SQL.Text := 'select * from xt_shengjwjqd where bianm=' +
          IntToStr(bianm);
      qryfile.Open;
      qryfile.Edit;
      blbfldqryfilewenj.LoadFromStream(filestream);
      qryfile.Post;
      qryfile.Close;
    except
      on E:Exception do
      begin
        raise Exception.Create('上传文件' + filename + '失败:' + e.message);
      end;
    end
  finally
    filestream.Free;
    filestream := nil;
  end;

end;

function Tfrmmain.init: Boolean;
begin
  Result := False;
        
  //连接服务数据库
  addresultstr('连接数据库...');
  if not connectDb then
  begin
    Exit;
  end;
  addresultstr('数据库连接成功！');

  addresultstr('读取升级信息...');
  if not readshengjinfo then
  begin
    baseobj.showerror('读取升级信息失败:' + errmsg);
  end
  else
  begin
    btnbegin.Enabled := True;
  end;
  addresultstr('成功读取升级信息！');

  Result := True;
end;

function Tfrmmain.readshengjinfo: Boolean;
var
  filepath, filename : string;
  config : TMyConfig;
  listfilename : string;
  i : Integer;
  str: string;
begin
  Result := False;

  try
    //$$$配置文件pmakeupdate.ini中读取上次升级序号
    filepath := ExtractFilePath(Application.ExeName) + '\';
    filename := filepath + 'pmakeupdate.ini';
    if not FileExists(filename) then
    begin
      errmsg := '升级配置文件未找到！';
      Exit;
    end;
    config := TMyConfig.Create(filename);
    //上次升级版本号
    fshengjxh_prev := config.ReadString('pmakeupdate', 'shengjxh_prev', '');


    //$$$数据库升级记录读取上次升级版本号
    qry.OpenSql('select isnull(max(shengjxh),0) from xt_shengjjl');

    //$$$检查最新升级包中记载的上次升级序号是否匹配数据库中的升级序号？
    if not SameText(qry.getString(0), fshengjxh_prev) then
    begin
      FreeAndNil(config);
      errmsg := '请按照系统补丁包顺序升级！';
      exit;
    end;
  
    //$$$读取本次升级序号
    fshengjxh := config.ReadString('pmakeupdate', 'shengjxh', '');
    if not baseobj.strisint(fshengjxh) then
    begin
      FreeAndNil(config);
      errmsg := '非法的升级序号！';
      Exit;
    end;
    lblshengjxh.Caption := '升级序号:' + fshengjxh;
    qry.OpenSql('select 1 from xt_shengjjl ' +
      ' where shengjxh=' + fshengjxh);
    if qry.RecordCount > 0 then
    begin
      if not baseobj.showdialog('系统升级', '升级序号对应的升级包已经存在,要重新升级吗？') then
      begin
        errmsg := '升级序号对应的升级包已经存在！';
        FreeAndNil(config);
        qry.Close;
        Exit;
      end
      else
      begin
        //删除上次的升级文件信息; 如果已经有客户端下载过当次升级文件，如何处理？
        //最好的办法是采取覆水难收，升级序号在发行的时候++即可；
        qry.ExecuteSql('delete from xt_shengjwjqd where shengjxh=' + fshengjxh);
        qry.ExecuteSql('delete from xt_shengjjl where shengjxh=' + fshengjxh);
      end;
    end;
    qry.Close;

    //$$$读取升级文件清单
    listfilename := config.ReadString('pmakeupdate','listfile','');
    if Trim(listfilename) = '' then
    begin
      FreeAndNil(config);
      errmsg := '升级清单文件未配置！';
      Exit;
    end;   
    if not FileExists(filepath + listfilename) then
    begin
      errmsg := '升级清单文件不存在！';
      Exit;
    end;          
  
    //$$$服务端升级后执行文件
    frunfilename := config.ReadString('pmakeupdate','runfile','');
    FreeAndNil(config);

    //升级文件加载升级文件清单
    lstfile.Items.LoadFromFile(filepath + listfilename);
    //文件是否存在检查
    for i:=0 to lstfile.Items.Count-1 do
    begin
      if Trim(lstfile.Items[i]) <> '' then
      begin
        if not FileExists(filepath + lstfile.Items[i]) then
        begin
          errmsg := '升级明细文件' + lstfile.Items[i] + '不存在！';
          Exit;
        end;
      end;
    end;

    Result := True;
  except
    on E:Exception do
    begin
      Result:= false;
      errmsg:= e.Message;
    end;
  end;   
end;

procedure Tfrmmain.backupdatabase;
var
  dbserber, dbuser, dbpassword, dbdatabase : string;
begin
  baseobj.getDbLoingIngo(adocon.ConnectionString, dbserber, dbuser, dbpassword, dbdatabase);
  qry.SQL.Text := 'backup database ' + dbdatabase + ' to disk=''' +
    fserver_path + '\upbefore_' + DateToStr(Now) + '.db.bak''';
  qry.ExecSQL;
end;

procedure Tfrmmain.addresultstr(strmsg: string);
begin
  mmo.Lines.Add(strmsg);
  Application.ProcessMessages;
end;

procedure Tfrmmain.exec_sql_file(filename: string);
var
  i : Integer;
  f : TextFile;
  strline : string;
begin 
  AssignFile(f, filename);
  Reset(f);     
  qry1.SQL.Text := '';
  
  while not Eof(f) do
  begin
    Readln(f, strline);
    strline := Trim(strline);
    strline := TrimLeft(strline);
    strline := TrimRight(strline);

    if SameText(LowerCase(strline), 'go') then
    begin
      addresultstr(qry1.SQL.Text);
      i := qry1.ExecSQL;
      addresultstr('qry1.exesql return:' +  inttostr(i));

      qry1.SQL.Text := '';
    end
    else if (Trim(strline) <> '') and (strline[1]<>'-') and
      (strline[1]<>'/') then
    begin
      qry1.SQL.Add(strline);
    end;
  end;
  CloseFile(f);
end;

end.
