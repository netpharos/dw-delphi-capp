unit uframeBase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ubase, StdCtrls;

type
  Tframebase = class(TFrame)
  private
  public
    baseobj : tbase;
    //公用规则对象

    errcode : integer;
    errmsg : string;
    //错误代码\错误描述
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

implementation

{$R *.dfm}

{ Tframebase }

constructor Tframebase.Create(AOwner: TComponent);
begin
  inherited;

  baseobj := tbase.Create;
end;

destructor Tframebase.Destroy;
begin
  FreeAndNil(baseobj);
  
  inherited;
end;

end.
