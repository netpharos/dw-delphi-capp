//*******************************************************************
//  ProjectGroup1  pfuzhuang   
//  udbconfigfile.pas     
//                                          
//  创建: changym by 2012-01-03 
//        changup@qq.com                    
//  功能说明:                                            
//      数据库配置文件的读取和写入;
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************

unit udbconfigfile;

interface   
uses
  Windows, SysUtils, Variants, Classes, ubase, utypes, uencode;

type
  TDBConfigFile = class(tbase)
  private
    ffilename : string;
    //数据库配置文件
  public
    constructor create(filename : string);
  public
    function readconfigstring(var buffer; var len : integer) : boolean;
    //读取配置串
    function writeconfigstring(const buffer; len :integer) : boolean;
    //写配置串
    
  end;

implementation

{ TDBConfigFile }

constructor TDBConfigFile.create(filename: string);
begin
  ffilename := filename;
end;

function TDBConfigFile.readconfigstring(var buffer;
  var len: integer): boolean;
var
  i, ilen : longint;
  f : TFileStream;     
  strencode : TEncode;
  buf : array[0..1024] of char;
begin
  Result := False;
  f := nil;

  try
    //检查配置文件是否存在
    if not fileexists(ffilename) then
    begin
      errcode := -1;
      errmsg :='配置文件' + ffilename + '不存在';
      exit;
    end;

    f := tfilestream.Create(ffilename, 0);
    if (f.Read(buffer, 4)) = -1 then
    begin
      errcode := -2;
      errmsg :='读取配置文件失败';
      exit;
    end;

    copymemory(@ilen, @buffer, 4);
    i := f.Read(buffer, ilen);

    if i<>ilen then
    begin
      errcode := -2;
      errmsg :='读取配置文件失败';
      exit;
    end;
    len := ilen;

    zeromemory(@buf, 1024);
    strencode := TEncode.Create;
    strencode.encode_xor(@buffer, len, buf, 'P');
    strencode.encode_xor(buf, len, @buffer, 'U');
    freeandnil(strencode);
  finally
    if Assigned(f) then
      f.Destroy;
  end;

  result := true;
end;

function TDBConfigFile.writeconfigstring(const buffer;
  len: integer): boolean;
var                                
  f : TFileStream;
  buffer1 : array[0..1024] of char; 
  buffer2 : array[0..1024] of char;
  strencode : TEncode;
begin
  zeromemory(@buffer1, 1024);
  zeromemory(@buffer2, 1024);
  
  strencode := TEncode.Create;
  strencode.encode_xor(@buffer, len, buffer1, 'U');
  strencode.encode_xor(buffer1, len, buffer2, 'P'); 
  freeandnil(strencode);

  f := TFileStream.Create(ffilename, fmCreate);
  copymemory(@buffer1, @len, 4);
  f.Write(buffer1, 4);
  f.WriteBuffer(buffer2,len);
  f.Destroy;

  Result := True;    
end;

end.
