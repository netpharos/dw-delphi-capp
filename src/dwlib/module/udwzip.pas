{*******************************************************}
{                                                       }
{       大维CS基础框架                                  }
{                                                       }
{       版权所有 (C) 2018 大维科技                      }
{                                                       }
{*******************************************************}

unit udwzip;

interface
      
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, zlib;

  function CompressionFile(sorfilename, desfilename: string): boolean;
  //压缩文件；此函数会抛出异常；
  //传入源文件和目标文件

  function DecompressionFile(sorfilename, desfilename: string): Boolean;
  //解压缩文件；此函数会抛出异常；
  //传入源文件和目标文件
             
implementation
          
function CompressionFile(sorfilename, desfilename: string): boolean;
var
  cs: TCompressionStream;
  fs,ms: TMemoryStream;
  num: Integer;
begin
  try
    Result:= False;

    if not FileExists(sorfilename) then
      raise Exception.Create('源文件不存在！');

    //读入源文件
    fs:= TMemoryStream.Create;
    fs.LoadFromFile(sorfilename);
    num:= fs.Size;

    //
    ms:= TMemoryStream.Create;
    ms.Write(num, SizeOf(num));        

    { 压缩比参数:
      clNone    无压缩
      clFastest 快速
      clDefault 默认
      clMax     最大比例 }
    cs:= TCompressionStream.Create(clMax, ms);
    fs.SaveToStream(cs);
    cs.Free;   
    ms.SaveToFile(desfilename);
    
    Result:= True;
  finally
    ms.Free;
    fs.Free;
  end;
end;
  
function DecompressionFile(sorfilename, desfilename: string): Boolean;
var
  ds: TDecompressionStream;
  fs,ms: TMemoryStream;
  num: Integer;
begin
  try
    Result:= False;

    //读入源文件
    fs:= TMemoryStream.Create;
    fs.LoadFromFile(sorfilename);

    //读出源文件大小
    fs.Position:= 0;
    fs.ReadBuffer(num, sizeof(num));

    //创建目标stream并设置大小
    ms:= TMemoryStream.Create;
    ms.SetSize(num);

    ds:= TDecompressionStream.Create(fs);
    ds.Read(ms.memory^, num);

    //保存
    ms.SaveToFile(desfilename);

    Result:= True;
  finally
    ds.Free;
    ms.Free;
    fs.Free;
  end;
end;

end.

