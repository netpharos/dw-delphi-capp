unit ukeyvaluelist;

interface  
uses
  Windows, SysUtils, Classes, Math, Types, DateUtils;
  
type
  { 系统参数类
    本身item存储key
  }
  TKeyValueList = class(TStringList)
  private
    values : TStringList;
    //存储参数值
    values_other : TStringList;
    //其他值

  public
    procedure additem(strkey, strvalue : string); overload;
    procedure additem(strkey, strvalue, strother : string); overload;
    function updateitemvalue(strkey, strvalue, strother : string) : boolean;
    function updateitemvalue_noadd(strkey, strvalue, strother : string) : boolean;
    //修改节点的值
    procedure clearitem();
    function valueitem(strkey : string) : string;
    function otheritem(strkey : string) : string;
    function getkey(index : integer) : string;
    function getvalue(index : integer) : string;
    function getother(index : integer) : string;

    function keyindexof(strkey : string) : integer;
    //指定的关键字是否已经存在, 用于唯一性检查
    function valueindexof(strvalue : string) : Integer;
    //指定的关键字值是否已经存在, 用于值唯一性检查
  public

    constructor Create();
    destructor Destroy; override;
  end;
  
implementation

{ TKeyValueList }

procedure TKeyValueList.additem(strkey, strvalue: string);
begin
  Add(strkey);
  values.Add(strvalue);
  values_other.Add('');
end;

procedure TKeyValueList.additem(strkey, strvalue, strother: string);
begin                    
  Add(strkey);
  values.Add(strvalue);
  values_other.Add(strother);
end;

procedure TKeyValueList.clearitem;
begin
  values.Clear;
  values_other.Clear;
  Clear;
end;

constructor TKeyValueList.Create;
begin   
  values := TStringList.Create;
  values_other := TStringList.Create;
end;

destructor TKeyValueList.Destroy;
begin
  values.Clear;
  values.Free;
  values := nil;

  values_other.Clear;
  values_other.Free;
  values_other := nil;

  inherited;
end;

function TKeyValueList.getkey(index: integer): string;
begin
  if index < 0 then Result := '';
  if index >= self.Count then Result := '';
  Result := self.Strings[index];
end;

function TKeyValueList.getother(index: integer): string;
begin
  if index < 0 then Result := '';
  if index >= self.values_other.Count then Result := '';
  Result := self.values_other.Strings[index];
end;

function TKeyValueList.getvalue(index: integer): string;
begin
  if index < 0 then Result := '';
  if index >= self.values.Count then Result := '';
  Result := self.values.Strings[index];
end;

function TKeyValueList.keyindexof(strkey: string): integer;
begin
  Result := IndexOf(strkey);
end;

function TKeyValueList.otheritem(strkey: string): string;
begin
  Result := '';
  if IndexOf(strkey) = -1 then Exit;

  Result := values_other.Strings[IndexOf(strkey)];
end;

function TKeyValueList.updateitemvalue(strkey, strvalue,
  strother: string) : boolean;
var
  i : integer;
begin
  Result := False;

  i := IndexOf(strkey);
  if i= -1 then Exit;

  values.Strings[i] := strvalue;
  values_other.Strings[i] := strother;

  Result := True;
end;

function TKeyValueList.updateitemvalue_noadd(strkey, strvalue,
  strother: string): boolean;
var
  i : integer;
begin
  Result := False;

  i := IndexOf(strkey);
  if i= -1 then
  begin
    additem(strkey, strvalue, strother);
    Result := True;
    Exit;
  end;

  values.Strings[i] := strvalue;
  values_other.Strings[i] := strother;

  Result := True;

end;

function TKeyValueList.valueindexof(strvalue: string): Integer;
begin
  Result := values.IndexOf(strvalue);
end;

function TKeyValueList.valueitem(strkey: string): string;
begin
  Result := '';
  if IndexOf(strkey) = -1 then Exit;

  Result := values.Strings[IndexOf(strkey)];
end;

end.
